import app.services.book_service as book_service
import unittest
from pathlib import Path
import json

""" A class that tests the retrieval of a list of all books """
class TestGetBookList(unittest.TestCase):
    """ Code that is run before every test. """
    def setUp(self):
        self.__book_service = book_service.BookService()
        with open(self.__get_path(), 'r') as rf:
            bookdata = json.loads(rf.read())
        self.bookdata = bookdata

    """ Tests if the get_all_books() method returns all book objects """
    def test_view_all_books(self):
        Books = {1: {"name": "Bee Book"}, 2: {"name": "Bop it, journey to greatness"}, 3: "Kellog's Special K"}
        self.assertEqual(self.__book_service.get_all_books(), self.bookdata)

    """
        Tests if the get_all_books() method doesn't return just an empty string
    """
    def test_compare_wrong_results(self):
        self.assertNotEqual(self.__book_service.get_all_books(),"")
    
    """
        Test if the length of the results of get_all_books() and the books
        stored in dummydata is the same.
    """
    def test_compare_length_of_results(self):
        result = self.__book_service.get_all_books()
        Blen = len(self.bookdata)
        self.assertEqual(len(result), Blen)
    
    def __get_path(self):
        base_path = Path(__file__).parent
        return (base_path / "../app/data/bookdata.json").resolve()


if __name__ == "__main__":
    unittest.main()