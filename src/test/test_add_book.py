import unittest
from app.services.book_service import BookService
from app.repositories.book_repository import BookRepository

""" Tests the add_book() function """
class testAddBook(unittest.TestCase):
    """ Sets up the test for each test method"""
    def setUp(self):
        self.__bookservice = BookService()
        self.__bookrepository = BookRepository()
        self.booklist = self.__bookservice.get_all_books()
        self.delete = False
        self.testbook = {
        "user_id": 2,
        "name": "Harry Potter and the Chamber of Secrets",
        "authors": ["J.K. Rowling"],
        "genres": ["Fantasy"],
        "published": 1999,
        "edition": 3,
        "condition": 0
}

        self.testbook_2 = {
            "user_id": 2,
            "edition": 4,
            "authors": ["my leg"],
            "genres": ["fantasy"],
            "published": 1999,
            "condition": 2}

    """ Cleans up for the next test method """
    def tearDown(self):
        # self.booklist = None
        # self.book = None
        if self.delete:
            self.__bookservice.hard_delete_book(self.__bookrepository.get_next_book_id() - 1)
            self.__bookservice.decrement_next_book_id()

    """ Tests if adding a valid book works correctly """
    def test_add_book(self):
        #this doesn't appear to be actually adding the book, despite not crashing at all
        #process is similar to add_user, consider comparing.
        numbbooks = len(self.booklist)
        self.__bookservice.add_book(self.testbook)
        self.delete = True
        dict_list = self.__bookservice.get_all_books()
        self.assertEqual(numbbooks+1, len(self.__bookservice.get_all_books()))

    #Both below fail because the program doesn't check for any incorrect input.
    """ Tests adding a book with a missing name """
    def test_add_book_missing_input(self):
        with self.assertRaises(ValueError):
            self.__bookservice.add_book(self.testbook_2)

    """ Tests adding a book that is equal to None """
    def test_add_book_none_book(self):
        self.book = None
        with self.assertRaises(ValueError):
            self.__bookservice.add_book(self.book)


if __name__ == "__main__":
    unittest.main()
    