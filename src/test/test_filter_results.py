import app.services.book_service as book_service
import unittest

""" A class that tests filtering the results """
class TestGetBookList(unittest.TestCase):
    """ Code that is run before every test. """
    def setUp(self):
        self.__book_service = book_service.BookService()


if __name__ == "__main__":
    unittest.main()