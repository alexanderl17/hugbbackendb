import unittest
from app.services import book_service
from app.repositories.book_repository import BookRepository


""" Tests the remove_book() function to make sure it is working as expected """
class TestRemoveBook(unittest.TestCase):

    """ Runs before every test case so they have a list of books """
    def setUp(self):
        self.__book_service = book_service.BookService()
        self.__book_repository = BookRepository()
        self.booklist = self.__book_service.get_all_books()
        #creating the book, every time
        self.fakebook = {
            "user_id": 1,
            "name": "How to Avoid Huge Ships (2nd. edition)",
            "authors": ["John W. Trimmer"], 
            "genres": ["Education"],
            "edition": 69,
            "published": 1993, 
            "condition": 0,
            }

        self.__book_service.add_book(self.fakebook)
        
        
    
    """  Runs after every test case, removes the list from setUp """
    def tearDown(self):
        #self.booklist = None
        #self.book = None
        #removing the book every time.
        # self.method.remove_book(4)
        self.__book_service.hard_delete_book(self.__book_repository.get_next_book_id()-1)
        self.__book_service.decrement_next_book_id()
    
    """
        Test to see if the function deletes the last book in bookdata.json
    """
    def test_delete_last_book(self):
        fake_id = self.__book_repository.get_next_book_id() - 1
        book_count = len(self.__book_service.get_all_books())
        self.__book_service.remove_book(fake_id)
        self.assertEqual(self.__book_service.get_book("How to Avoid Huge Ships (2nd. edition)")["status"], 3)

    """ Test removing a book that does not exist in the system. """
    def test_id_not_exists(self):
        fake_id = -5
        with self.assertRaises(KeyError):
            self.__book_service.remove_book(fake_id)

    """ Test removing a book with a wrong type of parameter value. """
    def test_invalid_parameter(self):
        fake_id = "b"
        book_count = len(self.booklist)
        self.__book_service.remove_book(fake_id) #should not remove any book
        self.assertEqual(book_count, len(self.booklist))

if __name__ == "__main__":
    unittest.main()
